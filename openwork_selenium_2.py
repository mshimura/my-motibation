# -*- coding: utf-8 -*-
"""
Created on Sat May 16 18:20:49 2020

@author: Masahiro
"""
import time
import requests
import re
from selenium import webdriver
from selenium.webdriver.chrome.options import Options
from bs4 import BeautifulSoup
from selenium.webdriver.common.action_chains import ActionChains
from selenium.webdriver.support.ui import Select
from xlwt import Workbook

URL = 'https://www.vorkers.com/login.php'
ID = 'mshimura27@gmail.com'
PASS = 'masahiro1127'

#ブラウザ起動
options = Options()
options.add_argument('--headless')
#driver = webdriver.Firefox('chromedriver.exe',chrome_options=options)
driver = webdriver.Firefox(r"C:\Users\Masahiro\Desktop\python study\Selenium practice")
driver.get(URL)

#ログインidを入れるところはソースコードから探す必要あるので以下のform~をあとで修正
driver.find_element_by_id("_username").send_keys(ID)
driver.find_element_by_id("_password").send_keys(PASS)
driver.find_element_by_id("log_in").click()

print('ログイン成功')
time.sleep(20)
driver.get('https://www.vorkers.com/company_answer.php?m_id=a0910000000FrT4&q_no=8')
res = requests.get('https://www.vorkers.com/company_answer.php?m_id=a0910000000FrT4&q_no=8')
# レスポンスの HTML から BeautifulSoup オブジェクトを作る
soup = BeautifulSoup(res.text, 'html.parser')

# 出力ファイル
filename_out = 'output_fanuc.txt'
out = open(filename_out,'w')

# HTMLタグを除去するためにHTMLタグの正規表現を設定
p = re.compile(r"<[^>]*?>")

# 403エラーが出たので、ひとまずHTMLファイルを読み込む方法をとる
#filename = "openwork_source.html"
#open関数の中でUTF-8を指定
#soup = BeautifulSoup(open(res,encoding="UTF-8"), 'html.parser')

# title タグの文字列を取得する
articles = soup.find_all('dd', class_='article_answer')
#articles = soup.find_all('dd', class_='article_answer')
for article in articles:
	# デバッグ用に標準出力
	#print(article)
	# HTMLタグを除去
	article_notag = p.sub("",str(article))
	# 文字型に変換してファイル出力
	out.write(article_notag + '\n')
